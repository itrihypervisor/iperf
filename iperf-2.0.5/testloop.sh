#!/bin/bash
echo "This test program will continuously test until miss the server report!"
count=1
while [ 1 ]; do
    echo -n "TEST $count "
    let count=$count+1
    src/iperf -n 100000 -l 100 -u -c localhost -a 10000 -e | grep "Server Report" || break
done
