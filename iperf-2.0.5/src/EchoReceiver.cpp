/*--------------------------------------------------------------- 
 * Copyright (c) 1999,2000,2001,2002,2003                              
 * The Board of Trustees of the University of Illinois            
 * All Rights Reserved.                                           
 *--------------------------------------------------------------- 
 * Permission is hereby granted, free of charge, to any person    
 * obtaining a copy of this software (Iperf) and associated       
 * documentation files (the "Software"), to deal in the Software  
 * without restriction, including without limitation the          
 * rights to use, copy, modify, merge, publish, distribute,        
 * sublicense, and/or sell copies of the Software, and to permit     
 * persons to whom the Software is furnished to do
 * so, subject to the following conditions: 
 *
 *     
 * Redistributions of source code must retain the above 
 * copyright notice, this list of conditions and 
 * the following disclaimers. 
 *
 *     
 * Redistributions in binary form must reproduce the above 
 * copyright notice, this list of conditions and the following 
 * disclaimers in the documentation and/or other materials 
 * provided with the distribution. 
 * 
 *     
 * Neither the names of the University of Illinois, NCSA, 
 * nor the names of its contributors may be used to endorse 
 * or promote products derived from this Software without
 * specific prior written permission. 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTIBUTORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 * ________________________________________________________________
 * National Laboratory for Applied Network Research 
 * National Center for Supercomputing Applications 
 * University of Illinois at Urbana-Champaign 
 * http://www.ncsa.uiuc.edu
 * ________________________________________________________________ 
 *
 * EchoReceiver.cpp
 * by Houcheng Lin <linhaocheng@itri.org.tw>
 * -------------------------------------------------------------------
 * A client thread initiates a connect to the server and handles
 * sending and receiving data, then closes the socket.
 * ------------------------------------------------------------------- */

#include "headers.h"
#include "EchoReceiver.hpp"
#include "Thread.h"
#include "SocketAddr.h"
#include "PerfSocket.hpp"
#include "Extractor.h"
#include "delay.hpp"
#include "util.h"
#include "Locale.h"
#include "Condition.h"
/* -------------------------------------------------------------------
 * Store server hostname, optionally local hostname, and socket info.
 * ------------------------------------------------------------------- */
extern "C" Condition EchoReceiverCond;
int EchoReceiver::clientSock;

void EchoReceiver::WakeEchoReceiver(int mSock) {
    EchoReceiver::clientSock = mSock;
    Condition_Signal(&EchoReceiverCond);
}

EchoReceiver::EchoReceiver( thread_Settings *inSettings ) {
    mSettings = inSettings;
    mBuf = new char[ mSettings->mBufLen ];
    reportstruct = new ReportStruct;
    mSettings->mThreadMode = kMode_Server;
    echoReport = InitReport( mSettings );
}

EchoReceiver::~EchoReceiver( ) {
    free(mBuf);
}

void EchoReceiver::Run( ) {
    Condition_Wait ( &EchoReceiverCond );
    mSettings->mSock = dup(EchoReceiver::clientSock);
    while(true) {
        int ret = checkServerResponse();
        if (ret < 0) {
            break;
        }
    }
    CloseReport( echoReport, reportstruct );
    EndReport(echoReport);
}

/* -------------------------------------------------------------------
 * Check server response, and update it into server response statistics.
 * In the end, check the statistics and print out the service disruption
 * time.
 * ------------------------------------------------------------------- */
int EchoReceiver::checkServerResponse() {
    int rc;
    struct UDP_datagram* mBuf_UDP = (struct UDP_datagram*) mBuf;
    fd_set readSet; 
    struct timeval timeout;
    // share the same socket and sometime it receierver later than client
    // event client has wait 0.5 second

again:
/*
    FD_ZERO( &readSet ); 
    FD_SET( mSettings->mSock, &readSet ); 
    timeout.tv_sec  = 0; 
    timeout.tv_usec = 1000; // quarter second, 250 ms 
    
    rc = select( mSettings->mSock+1, &readSet, NULL, NULL, &timeout);
    if(rc <= 0) {
        goto again;
    } */
    rc = read(mSettings->mSock, mBuf, mSettings->mBufLen);
    if (rc <= 0) {
        return rc;
    }
    rc = checkServerMessage(mBuf_UDP, rc);

    // wait reporter to handle last message
    if (rc < 0) {
        usleep(105*1000);
    }
    return rc;
}

int EchoReceiver::checkServerMessage(struct UDP_datagram* msg, int size) {
    int packetID;
    Timestamp now;
    now.setnow();
    packetID = htonl(msg->id);
    if (packetID < 0) {
        reportstruct->packetID = -packetID;
    } else {
        reportstruct->packetID = packetID;
    }
    reportstruct->packetLen = size;
    reportstruct->sentTime.tv_sec = htonl(msg->tv_sec);
    reportstruct->sentTime.tv_usec = htonl(msg->tv_usec);    
    reportstruct->packetTime.tv_sec = now.getSecs();
    reportstruct->packetTime.tv_usec = now.getUsecs();
    ReportPacket(echoReport, reportstruct);
    // A trick to not messy the socket connect messages.
    if (reportstruct->packetID ) {
        printf("Get echo message with id:%05d\r", packetID);
        if (packetID < 0) {
            printf("\nDone.\n");
        }
    }
    return packetID;
}
